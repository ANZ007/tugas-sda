#include "LLSingle.h"

Stack::Stack() {
    _head = _tail = NULL;
}

bool Stack::cari(char key) {
    Node *tmp;
    bool result = false;
    bool loop = true;

    tmp = _head;
    while (tmp != NULL) {
        if (key==tmp->data&&loop){
            result = true;
            loop=false;
        }
        tmp = tmp->next;
    }
    return result;
}

void Stack::cetak() {
    Node *tmp;

    tmp = _head;
    cout << "[";
    while (tmp != NULL){
        cout << tmp->data;
        if (tmp->next!=NULL){
            cout<<" , ";
        }
        tmp = tmp->next;
    }
    cout<<"]"<<endl;
}

void Stack::push(char x) {
    Node *tmp;

    tmp = new (Node);
    tmp->data = x;
    tmp->next = _head;
    _head = tmp;
}

char Stack::pop() {
    char result = '-1';
    Node *tmp;

    if (_head != NULL) {
        result = _head->data;
        tmp = _head;
        _head = tmp->next;
        delete(tmp);
    }

    return result;
}

bool Stack::isEmpty() {
    if (_head == NULL) {
        return true;
    }
    return false;
}

char Stack::tos() {
    if (_head != NULL) {
        return _head->data;
    }
}
