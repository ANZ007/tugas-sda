/** Lab 02 - Prak 2
  * 17 September 2019
  * Arviandri Naufal Zaki
  */

#include <iostream>

using namespace std;

#define JML 10

class Daftar {
    public:
        Daftar();
        void insert(int);
        int find(int);
        void remove();
        void cetak();
        void cetakSemua();
        bool isFull();
        bool isEmpty();
        void length();

    private:
        int _data[JML];
        int _jumlah;
};

Daftar::Daftar() {
    _jumlah = 0;
}



bool Daftar::isFull() {
    if(_jumlah == 10) {
        return true;
    } else {
        return false;
    }
}

bool Daftar::isEmpty() {
    if(_jumlah == 0) {
        return true;
    } else {
        return false;
    }
}

void Daftar::insert(int d) {
    if (isFull()) {
        cout << "Kapasitas Array Penuh" <<endl;
    } else {
        _data[_jumlah] = d;
        _jumlah++;
    }
}

void Daftar::remove() {
    if (isEmpty()) {
        cout << "Array Sudah Kosong, Tidak Bisa Remove" <<endl;
    } else {
        _jumlah--;
    }
}

void Daftar::cetak() {
    int j=_jumlah-1;
    //cout << "=========" <<endl;
    cout << "[";
    for (int i=0; i<j; i++) {
        //cout << "Data Array ke " <<j<< " : " << _data[i] <<endl;
        //j++;
        cout<< _data[i] <<", ";
    }
    cout << _data[j] << "]"<<endl;
}

void Daftar::cetakSemua() {
    int j = JML-1;
    //cout << "=========" <<endl;
    cout << "[";
    for(int i=0; i<JML-1; i++) {
        cout << _data[i] << " , ";
    }
    cout << _data[j] << "]" <<endl;
}

int Daftar::find(int x) {
    //cout << "=========" <<endl;
    bool finded = false;
    for (int i=0; i<_jumlah; i++) {
        if (_data[i] == x) {
            cout << x <<" berada pada index " << i <<endl;
            finded = true;
        }
    }
    if (finded == false) {
        cout << x <<" tidak ditemukan di dalam list"<<endl;
    }
    return 0;
}

void Daftar::length() {
    cout << "Jumlah data dalam list: "<<_jumlah<<endl;
}


int main() {
    Daftar daftar;

    daftar.insert(3);
    daftar.insert(4);
    daftar.insert(20);
    daftar.insert(21);
    daftar.insert(22);
    daftar.insert(23);
    daftar.insert(24);
    daftar.find(21);
    daftar.find(17);
    daftar.length();
    daftar.cetak();

    return 0;
}
