#ifndef LLSINGLE_H_INCLUDED
#define LLSINGLE_H_INCLUDED

#include <cstdlib>
#include <iostream>

using namespace std;

typedef struct data {
    char data;
    struct data *next;
} Node;

class Stack {
public:
    Stack();
    void push(char);
    void cetak();
    bool cari(char);
    char pop();
    bool isEmpty();
    char tos();

private:
    Node* _head;
    Node* _tail;
};

// function prototype
void push(char);
void cetak();
bool cari(char);
char pop();

#endif // LLSINGLE_H_INCLUDED
