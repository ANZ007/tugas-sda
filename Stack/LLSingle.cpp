#include "LLSingle.h"

Stack::Stack() {
    _head = _tail = NULL;
}

bool Stack::cari(int key) {
    Node *tmp;
    bool result = false;
    bool loop = true;

    tmp = _head;
    while (tmp != NULL) {
        if (key==tmp->data&&loop){
            result = true;
            loop=false;
        }
        tmp = tmp->next;
    }
    return result;
}

void Stack::cetak() {
    Node *tmp;

    tmp = _head;
    cout << "[";
    while (tmp != NULL){
        cout << tmp->data;
        if (tmp->next!=NULL){
            cout<<" , ";
        }
        tmp = tmp->next;
    }
    cout<<"]"<<endl;
}

void Stack::push(int x) {
    Node *tmp;

    tmp = new (Node);
    tmp->data = x;
    tmp->next = _head;
    _head = tmp;
}

int Stack::pop() {
    int result = -1;
    Node *tmp;

    if (_head != NULL) {
        result = _head->data;
        tmp = _head;
        _head = tmp->next;
        delete(tmp);
    }

    return result;
}

int main(int argc, char *argv[])
{
    Stack tumpukan;
    Node *list = NULL;
    int data;

    tumpukan.push(10);
    tumpukan.push(12);
    tumpukan.push(30);
    tumpukan.cetak();

    if (tumpukan.cari(2) == true)
        cout << "Ada angka 2" << endl;
    else
        cout << "Tidak ada angka 2" << endl;

    if (tumpukan.cari(10) == true)
        cout << "Ada angka 10" << endl;
    else
        cout << "Tidak ada angka 10" << endl;

    if (tumpukan.cari(17) == true)
        cout << "Ada angka 17" << endl;
    else
        cout << "Tidak ada angka 17" << endl;

    data = tumpukan.pop();
    cout << data << endl;

    return EXIT_SUCCESS;
}
