#include <iostream>
#include <string>
#include <vector>
#include <sstream>
//#include <stack>
#include "LLSingle.cpp"
using namespace std;
//#include "LLSingle.h"

int prec(char c)
{
    if(c == '^')
    return 3;
    else if(c == '*' || c == '/')
    return 2;
    else if(c == '+' || c == '-')
    return 1;
    else
    return -1;
}

// The main function to convert infix expression
//to postfix expression
void infixToPostfix(string s)
{
    //std::stack<char> st;
    Stack st;
    Node *list = NULL;
    st.push('N');
    int l = s.length();
    string ns;
    for(int i = 0; i < l; i++)
    {
        // If the scanned character is an operand, add it to output string.
        if((s[i] >= 'a' && s[i] <= 'z')||(s[i] >= 'A' && s[i] <= 'Z'))
        ns+=s[i];

        // If the scanned character is an ‘(‘, push it to the stack.
        else if(s[i] == '(')

        st.push('(');

        // If the scanned character is an ‘)’, pop and to output string from the stack
        // until an ‘(‘ is encountered.
        else if(s[i] == ')')
        {
            while(st.tos() != 'N' && st.tos() != '(')
            {
                char c = st.tos();
                st.pop();
               ns += c;
            }
            if(st.tos() == '(')
            {
                char c = st.tos();
                st.pop();
            }
        }

        //If an operator is scanned
        else{
            while(st.tos() != 'N' && prec(s[i]) <= prec(st.tos()))
            {
                char c = st.tos();
                st.pop();
                ns += c;
            }
            st.push(s[i]);
        }

    }
    //Pop all the remaining elements from the stack
    while(st.tos() != 'N')
    {
        char c = st.tos();
        st.pop();
        ns += c;
    }

    cout << "Ekspresi Postfix : " << ns << endl;

}

int main(){

    string infix;
    vector<string> tokens;
    string token;
    char delim = ' ';

    cout << "Masukkan ekspresi infix: ";
    getline(cin, infix);
    istringstream ss(infix);

    while (std::getline(ss, token, delim)) {
        tokens.push_back(token);
    }
    cout << "Jumlah token: " << tokens.size() << endl;

    // Printing the token vector
    for(int i = 0; i < tokens.size(); i++) {
        cout << i << ": " << tokens[i] << '\n';
    }
    string exp = "";
    for(int i = 0; i < tokens.size(); i++) {
        exp += tokens[i];
    }
    infixToPostfix(exp);
}
