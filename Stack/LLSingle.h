#ifndef LLSINGLE_H_INCLUDED
#define LLSINGLE_H_INCLUDED

#include <cstdlib>
#include <iostream>

using namespace std;

typedef struct data {
    int data;
    struct data *next;
} Node;

class Stack {
public:
    Stack();
    void push(int);
    void cetak();
    bool cari(int);
    int pop();

private:
    Node* _head;
    Node* _tail;
};

// function prototype
void push(int);
void cetak();
bool cari(int);
int pop();

#endif // LLSINGLE_H_INCLUDED
